Notes.md

Formation CSS test Dw4 - Central
================================

Bienvenue dans le support de cours CSS. 

[PixelPerfect]: https://chrome.google.com/webstore/detail/perfectpixel-by-welldonec/dkaagdgjmgdmbnecmcefdhjekcoceebi?hl=fr
[LiveReload]: https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=fr
[Imanexpert]: https://www.youtube.com/watch?time_continue=9&v=BKorP55Aqvg
[Media Queries]: https://developer.mozilla.org/fr/docs/Web/CSS/Requ%C3%AAtes_m%C3%A9dia/Utiliser_les_Media_queries

TODO : Trouver un canal de tchat collectif (Télégraph ?)

#Plan
 0. Présentation de qui je suis. 

    Bonjour, 

    Bienvenue dans le cours de CSS. 

    C'est la première fois que nous faisons cours ensemble. Je voudrais me présenter pendant 2-3 minutes pour informer au personne avec qui je n'ai pas eu encore l'occasion d'échanger. Même si je vous avoue que je ne suis jamais trop à l'aise pour parler de moi. 

    Je m'appelle Jérémy, j'ai 31ans, je vis à Nantes depuis maintenant 3ans, j'ai habité en général dans l'ouest de la France, La Rochelle, Rennes, Paris, (Le Havre), Nantes.  

    J'ai un parcours d'études assez multidisciplinaire un peu comme vous. J'ai étudié la mécanique industriel, La physique appliquée, la physique théorique, les sciences de l'information et de la communication pour en arriver au développement informatique. Mieux vaut tard que jamais.

    Je me suis rendu compte que contrairement au sciences dur j'avais besoin d'une activité plus créative. 

    . J'ai regardé un peu vos CV et j'ai vu qu'il avait des personnes parmi nous de région assez divers. Les cours de css vont être drôle car il va falloir laisser cours un peu à votre imagination avec des outils que vous n'avez pas encore l'habitude d'utiliser mais qui vous risque de devenir un de vos outils quotidiens.


 
 0.5 Introduction 

    Aujourd'hui on va parler de css. c'est un language qui est utilisé par les moteurs de rendu des navigateurs web (ex : Gecko ou webkit) pour décorer les pages web. 

 1. **L'intégrateur** 
    - Sa place dans la chaine de production (Graphiste --> **Intégrateur** --> Développeur Logiciel Front --> Développeur Logiciel Back --> DevOps)
        - Objectif : Savoir utiliser les termes usuels de la description d'interface graphique web.  
        - Explication sur la diversité des interlocuteurs (intégrateur, chef de projet, clients, dev front, dev back), les equipes mulitidisciplinaire.
        - Exercice oral : Description d'une page web classique (Exemple AirBnB) --> Réutilisation des termes usuel (header, footer, ...)
    - Plus tard --> [Imanexpert]
    - Les outils de l'integrateur (HTML, CSS, JavaScript, Outils de création image, son, vidéo)
        - Objectif : Savoir faire la différence entre le html, le css et javascript
        - Exercice oral : Description d'une page web classique (Exemple AirBnB), Découpage par fonctionnel (actions, éléments statique, éléments dynamique)
    
 2. **Le CSS, à quoi ça sert ?**
    - Le lien avec le HTML (Choisi historiquement par Netscape avec le html et javascript pour devenir le premier navigateur publique)
    - Les alternatives à CSS pour le rendu visuel (SVG, WebGL, Canvas, "Images")
        - Montrer des exemples 
    - L'évolution de CSS (passage des la version 2.1 à 3 avec plus possibilité et aujourd'hui en partie respecté par tous les navigateurs)
    - L'intégrateur, quelle place dans la chaine de développement?  Connaitre l'environnement de travail, connaitre de la limite de la tedchnologie. connaître les technologie à utiliser, Savoir déterminer de la complxité d'une tâche. 

 3. **Exemple simple : step 0**
    - Les différents type d'intégration dans une page HTML
        - balise style : ```<style></style>```
        - inline css : ```<span style="color:red">hello</span>```
        - ```<link rel="stylesheet" href="./index.css">```
    - Mise en situation (Bollerplate)
        Mise en place d'une premier page html avec css
    - Live coding à partir d'un fichier HTML
        Exemple du bouton simple
            - button par défaut (<button> in input type button)
            - utilisation des propriété pour modifier une balise span et le faire ressembler au rendu navigateur
            - utilisation d'une balise checkbox pour modifier le comportement visuel
                - Exercice --> Refaire le comportement de la checkbox avec une balise radio.
            - rendre plus esthétique le bouton 


 4. **Les outils de développement** 
    - Les différentes versions de CSS, l'histoire de CSS
    - la compatibilité (CanIUse : https://caniuse.com/)
    - [LiveReload]
    - La console de développement du navigateur
    - [PixelPerfect]
    - IDE et Autocompletion 

 5. **Les règles de base** 
    - le système de cascade
    - Les selecteurs
        - A quoi ça sert ? 
        - id vs class
        - examples selecteurs, pseudo-selecteurs et attribues (pseudo-class)
        - règles des priorités des selecteurs, le mot clé !important 
    - Les propriétés
        - background
        - text (fonts, colors, ...)
        - block
        - dimension (pixel, %)
        - display

 6. **Le positionnement**
    - Les différents positionnement (static, relative, absolute, sticky, fixed)
    - le positionnement des éléments par défaut en fonction de l'élément HTML
    - la propriété : float
    - la propriété : display
    - la propriété : z-index

 7. **Les layouts**
    - Différents types de grille (float, grid css, flexbox)
    - Le cas flexbox
    - Organisez sa page sous forme de layout

 8. **Le responsive design**
    - Adapter son contenu en fonction des écrans et des rendu
    - [Media Queries]

 9. **Twitter Bootstrap**
    - Présentation
    - Installation
    - Manipulation

 10. **Pratiques avancées**
    - Bien organiser son projet css
    - Les variables css pour créer des thèmes
    - la fonctionnalité calc
    - les animations
    - Exemples scss, less 
