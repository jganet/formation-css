Formation CSS test Dw4 - Central
================================

Bienvenue dans le support de cours CSS. 

[PixelPerfect]: https://chrome.google.com/webstore/detail/perfectpixel-by-welldonec/dkaagdgjmgdmbnecmcefdhjekcoceebi?hl=fr
[LiveReload]: https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=fr
[Imanexpert]: https://www.youtube.com/watch?time_continue=9&v=BKorP55Aqvg
[Media Queries]: https://developer.mozilla.org/fr/docs/Web/CSS/Requ%C3%AAtes_m%C3%A9dia/Utiliser_les_Media_queries

#Plan

 1. **L'intégrateur** 
    - Sa place dans la chaine de production (Graphiste --> **Intégrateur** --> Développeur Logiciel Front --> Développeur Logiciel Back --> DevOps)
    - [Imanexpert]
    - Les outils de l'integrateur (HTML, CSS, JavaScript, Outils de création graphique)
    
 2. **Le CSS, à quoi ça sert ?**
    - Le lien avec le HTML
    - Les alternatives à CSS
    - L'évolution de CSS
    - L'intégrateur, quelle place dans la chaine de développement? 

 3. **Exemple simple** 
    - Les différents type d'intégration dans une page HTML
        - balise style : ```<style></style>```
        - inline css : ```<span style="color:red">hello</span>```
        - the good one : ```<link rel="stylesheet" href="./index.css">``` --> Meilleur pour la minification
    - Mise en situation
    - Live coding à partir d'un fichier HTML

 4. **Les outils de développement** 
    
    - la compatibilité (CanIUse : https://caniuse.com/)
    - [LiveReload]
    - La console de développement du navigateur
    - [PixelPerfect]
    - IDE et Autocompletion 

 5. **Les règles de base**

    - le système de cascade
    - Les selecteurs
        - A quoi ça sert ? 
        - id vs class
        - examples selecteurs, pseudo-selecteurs et attribues (pseudo-class)
        - règles des priorités des selecteurs, le mot clé !important 
    - Les propriétés
        - background
        - text (fonts, colors, ...)
        - block
        - dimension (pixel, %)
        - display

 6. **Le positionnement**
    - Les différents positionnements (static, relative, absolute, sticky, fixed)
    - le positionnement des éléments par défaut en fonction de l'élément HTML
    - la propriété : float
    - la propriété : display
    - la propriété : z-index

 7. **Les layouts**
    - Différents types de grille (float, grid css, flexbox)
    - Le cas flexbox
    - Organiser sa page sous forme de layout

 8. **Le responsive design**
    - Adapter son contenu en fonction des écrans et des rendu
    - [Media Queries]

 9. **Twitter Bootstrap**
    - Présentation
    - Installation
    - Manipulation

 10. **Pratiques avancées**
    - Bien organiser son projet css
    - Les variables css pour créer des thèmes
    - la fonctionnalité calc
    - les animations
    - Exemples scss, less 
